Rails.application.routes.draw do

  get '/session/login' => 'session#login'
  post '/session/login' => 'session#check'

  get '/session/welcome' => 'session#welcome'
  post '/session/welcome' => 'session#welcome'

  get '/session/logout' => 'session#logout'

  root 'pages#home'
  get '/about' => 'pages#about'
  get '/user/show' => 'user#show'

  get '/book/list' => 'book#list'
  get '/book/show' => 'book#show'
  delete '/book/delete' => 'book#delete'

  get '/book/new' => 'book#new'
  post '/book/create' => 'book#create'

  get '/book/edit' => 'book#edit'
  patch '/book/update' => 'book#update'

  get '/book/subjects' => 'book#show_subjects'
end
