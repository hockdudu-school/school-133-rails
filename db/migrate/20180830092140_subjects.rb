class Subjects < ActiveRecord::Migration[5.2]
  def self.up
    create_table :subjects do |t|
      t.column :name, :string
    end

    Subject.create name: 'Rails'
    Subject.create name: 'Linux'
    Subject.create name: 'SQL'
    Subject.create name: 'Python'
    Subject.create name: 'Bash'
  end

  def self.down
    drop_table :subjects
  end
end
