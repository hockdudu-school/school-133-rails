class SessionController < ApplicationController
  def login
    user = User.find_by id: session[:user]
    redirect_to action: 'welcome' if user
  end

  def check
    @user = User.find_by username: params[:user][:username]

    if !@user.nil?
      if @user.pass == params[:user][:pass]
        session[:user] = @user.id
        redirect_to action: 'welcome'
        return
      else
        @error = 'pass'
      end
    else
      @error = 'username'
    end

    render 'login'
  end

  def welcome
    @user = User.find_by id: session[:user]
  end

  def logout
    session[:user] = nil
    redirect_to action: 'login'
  end
end
