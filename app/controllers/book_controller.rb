class BookController < ApplicationController
  def list
    @books = Book.all
  end

  def show
    @book = Book.find(params[:id])
  end

  def new
    @book = Book.new
    @subjects = Subject.all
  end

  def create
    @book = Book.new(book_params)

    if @book.save
      redirect_to action: 'list'
    else
      @subjects = Subject.all
      render action: 'new'
    end
  end


  def edit
    @book = Book.find(params[:id])
    @subjects = Subject.all
  end

  def update
    @book = Book.find(params[:id])
    if @book.update_attributes(book_param)
      redirect_to action: 'show', id: @book
    else
      @subjects = Subject.all
      render action: 'edit'
    end
  end

  def delete
    Book.find(params[:id]).destroy
    redirect_to action: 'list'
  end

  def show_subjects
    @subject = Subject.find(params[:id])
  end


  def book_param
    params.require(:book).permit(:title, :price, :subject_id, :description)
  end

  def book_params
    params.require(:books).permit(:title, :price, :subject_id, :description)
  end
end

# Book.create title: 'Rails Guides', price: 21, subject_id: 1, description: ''
# Book.create title: 'Linux Administration', price: 56, subject_id: 2, description: ''
# Book.create title: 'SQL Database', price: 12, subject_id: 3, description: ''
# Book.create title: 'Python for Dummies', price: 9, subject_id: 4, description: ''
# Book.create title: 'Bourne Again Shell', price: 10, subject_id: 5, description: 'Bash was first!'
# Book.create title: 'Linux Server', price: 13, subject_id: 2, description: 'Linux is faster!'